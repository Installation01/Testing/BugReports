Issue tracker is **ONLY** used for reporting bugs.

## Overview
**Short Description**
<!-- Provide a short general summary of the issue of the title above (Expand the title a little) -->


**Detailed Description**
<!-- Provide a more detailed summary of the issue that assist in the explanation


## Current Behavior
<!-- Tell us what happens instead of the expected behavior -->

## Expected Behavior
<!-- Tell us what should happen -->


## Steps to Reproduce
<!-- Provide a detailed list of steps required to reproduce this bug -->

1.
2.
3.
4.

<!-- A short video is required for a sufficient bug report -->
**Video**

Link: 

## Additional Information

<!-- Any additional information and/or suggestions regarding this bug that may help in diagnosing and solving the issue -->
